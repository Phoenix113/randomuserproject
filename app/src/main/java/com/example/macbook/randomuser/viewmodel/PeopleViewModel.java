package com.example.macbook.randomuser.viewmodel;


import com.example.macbook.randomuser.callback.FetcherCallback;
import com.example.macbook.randomuser.network.PeopleFetcher;


public class PeopleViewModel {
    private FetcherCallback callback;
    private int page = 0;


    public PeopleViewModel(FetcherCallback callback) {
        this.callback = callback;
    }

    public void fetchPeople() {
        page++;
        PeopleFetcher peopleFetcher = new PeopleFetcher(callback);
        peopleFetcher.fetchPeopleList("20", Integer.toString(page));

    }
}

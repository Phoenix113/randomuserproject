package com.example.macbook.randomuser.network;

import android.support.annotation.NonNull;

import com.example.macbook.randomuser.callback.FetcherCallback;
import com.example.macbook.randomuser.network.model.People;
import com.example.macbook.randomuser.network.model.Person;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PeopleFetcher {

    private FetcherCallback callback;
    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(PeopleApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private PeopleApi api = retrofit.create(PeopleApi.class);

    public PeopleFetcher(FetcherCallback callback) {
        this.callback = callback;
    }

    public void fetchPeopleList(String resultsNumber, String pageNumber) {

        // you can include any field here
        Call<People> peopleList = api.fetchPeople("name,location,phone,picture", resultsNumber, pageNumber);

        peopleList.enqueue(new Callback<People>() {
            @Override
            public void onResponse(@NonNull Call<People> call, @NonNull Response<People> response) {
                List<Person> peopleList = Objects.requireNonNull(response.body()).getPeopleList();
                callback.peopleFetched(peopleList);
            }

            @Override
            public void onFailure(@NonNull Call<People> call, @NonNull Throwable t) {
            }
        });
    }
}

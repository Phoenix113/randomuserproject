package com.example.macbook.randomuser.callback;

import com.example.macbook.randomuser.network.model.Person;

import java.util.List;

public interface FetcherCallback {
    void peopleFetched(List<Person> fetchedPeople);
}

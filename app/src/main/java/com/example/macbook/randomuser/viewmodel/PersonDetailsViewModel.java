package com.example.macbook.randomuser.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.macbook.randomuser.network.model.Person;

public class PersonDetailsViewModel extends BaseObservable {
    private Person person;


    public PersonDetailsViewModel(Person person) {
        this.person = person;
    }

    public void setPerson(Person person) {
        this.person = person;
        notifyChange();
    }

    public String getFullName() {
        return person.getName().getTitle() + " " + person.getName().getFirst() + " " + person.getName().getLast();
    }

    public String getStateCity() {
        return person.getLocation().getState() + "\t\t\t" + person.getLocation().getCity();
    }

    public String getPhoneNumber() {
        return person.getPhone();
    }

    public String getAvatarUrl() {
        return person.getPicture().getLarge();
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String imageUrl) {
        Glide.with(imageView.getContext()).load(imageUrl).into(imageView);
    }
}

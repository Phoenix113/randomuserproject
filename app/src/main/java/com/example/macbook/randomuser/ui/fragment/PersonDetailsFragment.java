package com.example.macbook.randomuser.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.macbook.randomuser.R;
import com.example.macbook.randomuser.databinding.FragmentPersonDetailsBinding;
import com.example.macbook.randomuser.network.model.Person;
import com.example.macbook.randomuser.viewmodel.PersonDetailsViewModel;

public class PersonDetailsFragment extends Fragment {

    public static final String ARG_PERSON = "person";

    private FragmentPersonDetailsBinding binding;
    private Person person;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_person_details, container, false);

        if (getArguments() != null) {
            person = (Person) getArguments().getSerializable(ARG_PERSON);
            bindDetails();
        }
        return binding.getRoot();
    }

    private void bindDetails() {
        if (binding.getPersonDetailsViewModel() == null) {
            binding.setPersonDetailsViewModel(
                    new PersonDetailsViewModel(person));
        } else {
            binding.getPersonDetailsViewModel().setPerson(person);
        }
    }

    /**
     * intended for the separate injection of person
     * used in the case of a tablet device
     */
    public void showPerson(Person person) {
        this.person = person;
        bindDetails();
    }
}

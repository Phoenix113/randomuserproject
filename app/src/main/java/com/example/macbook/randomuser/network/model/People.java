package com.example.macbook.randomuser.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class People {

    @SerializedName("results") private List<Person> peopleList;
    @SerializedName("info") private Info info;

    public List<Person> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(List<Person> mPeopleList) {
        this.peopleList = mPeopleList;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }


}

package com.example.macbook.randomuser.ui.activity;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.macbook.randomuser.R;
import com.example.macbook.randomuser.callback.ItemClickedCallback;
import com.example.macbook.randomuser.network.model.Person;
import com.example.macbook.randomuser.ui.fragment.AllPeopleFragment;
import com.example.macbook.randomuser.ui.fragment.PersonDetailsFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupFragments();
    }

    private void setupFragments() {
        final FragmentManager fragmentManager = getSupportFragmentManager();

        AllPeopleFragment allPeopleFragment = (AllPeopleFragment) fragmentManager.findFragmentById(R.id.allPeopleFragment);
        allPeopleFragment.setItemSelectedCallback(new ItemClickedCallback() {
            @Override
            public void itemClicked(Person person) {
                PersonDetailsFragment personDetailsFragment =
                        (PersonDetailsFragment) fragmentManager.findFragmentById(R.id.personDetailFragment);
                //phone case, as fragment is not found
                if (personDetailsFragment == null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(PersonDetailsFragment.ARG_PERSON, person);
                    personDetailsFragment = new PersonDetailsFragment();
                    personDetailsFragment.setArguments(bundle);
                    fragmentManager.beginTransaction()
                            .add(R.id.allPeopleFragment, personDetailsFragment)
                            .addToBackStack(null)
                            .commit();
                } else {
                    personDetailsFragment.showPerson(person);
                }
            }
        });
            fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(
                            fragmentManager.getBackStackEntryCount() > 0);
                }
            });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

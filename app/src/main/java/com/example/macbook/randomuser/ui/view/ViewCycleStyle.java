package com.example.macbook.randomuser.ui.view;

public class ViewCycleStyle {


        public static final class styleable {
            static final int[] CircleImageView = { 0x7f030088, 0x7f030089, 0x7f03008a, 0x7f03008b, 0x7f03008c };
            static final int CircleImageView_civ_border_color = 0;
            static final int CircleImageView_civ_border_overlay = 1;
            static final int CircleImageView_civ_border_width = 2;
            static final int CircleImageView_civ_circle_background_color = 3;
            static final int CircleImageView_civ_fill_color = 4;
        }
}

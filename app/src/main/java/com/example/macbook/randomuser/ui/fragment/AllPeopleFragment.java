package com.example.macbook.randomuser.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.macbook.randomuser.R;
import com.example.macbook.randomuser.callback.FetcherCallback;
import com.example.macbook.randomuser.callback.ItemClickedCallback;
import com.example.macbook.randomuser.callback.OnBottomReachCallback;
import com.example.macbook.randomuser.databinding.FragmentAllPeopleBinding;
import com.example.macbook.randomuser.network.model.Person;
import com.example.macbook.randomuser.ui.adapter.PeopleAdapter;
import com.example.macbook.randomuser.viewmodel.PeopleViewModel;

import java.util.List;

public class AllPeopleFragment extends Fragment implements FetcherCallback {

    private PeopleAdapter peopleAdapter;
    private FragmentAllPeopleBinding binding;
    private PeopleViewModel peopleViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_all_people, container, false);
        View view = binding.getRoot();
        peopleViewModel = new PeopleViewModel(this);
        peopleViewModel.fetchPeople();
        setPeopleAdapter();
        return view;
    }

    public void setItemSelectedCallback(ItemClickedCallback callback) {
        peopleAdapter.setItemClickedCallback(callback);
    }

    private void setPeopleAdapter() {
        peopleAdapter = new PeopleAdapter();
        binding.peopleRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.peopleRecyclerView.setAdapter(peopleAdapter);
        peopleAdapter.setOnBottomReachListener(new OnBottomReachCallback() {
            @Override
            public void bottomReached(int position) {
                binding.progressPeople.setVisibility(View.VISIBLE);
                peopleViewModel.fetchPeople();
            }
        });
    }

    @Override
    public void peopleFetched(List<Person> fetchedPeople) {
        binding.progressPeople.setVisibility(View.INVISIBLE);
        peopleAdapter.addToList(fetchedPeople);
    }
}

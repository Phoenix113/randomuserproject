package com.example.macbook.randomuser.callback;

public interface OnBottomReachCallback {
    void bottomReached(int position);
}

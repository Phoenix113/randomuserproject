package com.example.macbook.randomuser.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.graphics.Color;

import com.bumptech.glide.Glide;
import com.example.macbook.randomuser.network.model.Person;
import com.example.macbook.randomuser.ui.view.CycleImageView;

public class ItemPersonViewModel extends BaseObservable {
    private Person person;

    public ItemPersonViewModel(Person person) {
        this.person = person;
    }

    public String getFullName() {
        return person.getName().getTitle() + " " + person.getName().getFirst() + " " + person.getName().getLast();
    }

    public String getStateCity() {
        return person.getLocation().getState() + "\t\t\t" + person.getLocation().getCity();
    }

    public String getPhoneNumber() {
        return person.getPhone();
    }

    public void setPeople(Person person) {
        this.person = person;
        notifyChange();
    }

    public int getBgColor() {
        return person.isRead() ? Color.parseColor("#CCCCCC") : 0;
    }

    public String getAvatarUrl() {
        return person.getPicture().getLarge();
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(CycleImageView imageView, String imageUrl) {
        Glide.with(imageView.getContext()).load(imageUrl).into(imageView);
    }
}

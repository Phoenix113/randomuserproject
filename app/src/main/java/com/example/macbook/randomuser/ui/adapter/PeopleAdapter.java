package com.example.macbook.randomuser.ui.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.macbook.randomuser.R;
import com.example.macbook.randomuser.callback.ItemClickedCallback;
import com.example.macbook.randomuser.callback.OnBottomReachCallback;
import com.example.macbook.randomuser.databinding.ItemPersonBinding;
import com.example.macbook.randomuser.network.model.Person;
import com.example.macbook.randomuser.viewmodel.ItemPersonViewModel;

import java.util.ArrayList;
import java.util.List;

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.ViewHolder> {

    private List<Person> peopleList;
    private OnBottomReachCallback onBottomReachCallback;
    private ItemClickedCallback callback;

    public PeopleAdapter() {
        this.peopleList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemPersonBinding personBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_person, viewGroup, false);
        return new ViewHolder(personBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        viewHolder.bindPeople(peopleList.get(i));

        if (i == peopleList.size() - 1) {
            onBottomReachCallback.bottomReached(i);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = viewHolder.getAdapterPosition();
                Person person = peopleList.get(position);
                person.setRead(true);
                notifyItemChanged(position);
                callback.itemClicked(person);
            }
        });

    }

    public void addToList(List<Person> peopleList) {
        this.peopleList.addAll(peopleList);
        notifyItemRangeChanged(this.peopleList.size() - peopleList.size(), this.peopleList.size());
    }

    @Override
    public int getItemCount() {
        return peopleList.size();
    }

    public void setOnBottomReachListener(OnBottomReachCallback onBottomReachCallback) {
        this.onBottomReachCallback = onBottomReachCallback;
    }

    public void setItemClickedCallback(ItemClickedCallback itemClickedCallback) {
        this.callback = itemClickedCallback;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemPersonBinding itemPersonBinding;

        ViewHolder(@NonNull ItemPersonBinding itemPersonBinding) {
            super(itemPersonBinding.itemPerson);
            this.itemPersonBinding = itemPersonBinding;
        }

        void bindPeople(Person people) { // change Name of method
            if (itemPersonBinding.getItemPersonViewModel() == null) {
                itemPersonBinding.setItemPersonViewModel(
                        new ItemPersonViewModel(people));
            } else {
                itemPersonBinding.getItemPersonViewModel().setPeople(people);
            }
        }
    }
}

package com.example.macbook.randomuser.network;

import com.example.macbook.randomuser.network.model.People;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PeopleApi {

    String BASE_URL = "https://randomuser.me/";


    @GET("api/")
    Call<People> fetchPeople(@Query("inc") String includedFields, @Query("results") String number, @Query("page") String page);

}

package com.example.macbook.randomuser.callback;

import com.example.macbook.randomuser.network.model.Person;

public interface ItemClickedCallback {
    void itemClicked(Person person);
}
